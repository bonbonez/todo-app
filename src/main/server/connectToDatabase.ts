import {BOOLEAN, INTEGER, Sequelize, TEXT} from 'sequelize';
import {ITodo} from '../domain';

interface IDatabaseApi {
  readTodos(): Promise<Array<ITodo>>;
  createTodo(description: string): Promise<ITodo>;
  toggleTodoCompleted(id: number): Promise<ITodo>;
}

export async function connectToDatabase(): Promise<IDatabaseApi> {

  const sequelize = new Sequelize({
    dialect: 'sqlite',
    database: 'demo',
    storage: 'demo.db'
  });

  // TODO: remove `any` when the TS error is fixed in the `sequelize` package.
  const Todo: any = sequelize.define('todo', {
    id: {
      type: INTEGER,
      autoIncrement: true,
      primaryKey: true,
      allowNull: false,
    },
    description: {
      type: TEXT,
      allowNull: false,
    },
    completed: {
      type: BOOLEAN,
      allowNull: false,
      defaultValue: false,
    }
  });
  await sequelize.sync();

  return {
    readTodos() {
      return Todo.findAll({raw: true});
    },
    async createTodo(description) {
      try {
        const todo = await Todo.create({description});
        return todo.get({plain: true});
      } catch (e) {
        console.error(e);
      }
    },
    async toggleTodoCompleted(id) {
      const todo = await Todo.findByPk(id);
      if (todo) {
        const updatedTodo = await todo.update({completed: !todo.get({plain: true}).completed});
        return updatedTodo.get({plain: true});
      }
      return false;
    }
  }
}

