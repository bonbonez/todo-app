import express from 'express';
import cors from 'cors';
import {connectToDatabase} from './connectToDatabase';
import {ApiResponse, IApiResponseBody} from '../core';
import config from '../../../config.json';
import {ITodo} from '../domain';

interface ITodosPathParams {
  todoId: string;
  [key: string]: string; // ← this is required by express types.
}

startApi();

async function startApi() {

  console.info('Staring the API server...');

  const databaseApi = await connectToDatabase();
  const app = express();

  app.use(express.json());
  app.use(cors())

  app.get<never, IApiResponseBody<Array<ITodo>>>('/todos', async (req, res) => {
    return res.status(200).json({
      data: await databaseApi.readTodos()
    });
  });

  app.post<never, IApiResponseBody<ITodo>>('/todos', async (req, res) => {
    if (req.body.description) {
      return createOkResponse(res, await databaseApi.createTodo(req.body.description))
    } else {
      return createErrorResponse(res, 'invalid_description');
    }
  });

  app.post<ITodosPathParams, IApiResponseBody<ITodo>>('/todos/:todoId/toggle-completed', async (req, res) => {
    const todo = await databaseApi.toggleTodoCompleted(parseInt(req.params.todoId, 10));
    if (todo) {
      return createOkResponse(res, todo);
    } else {
      return createErrorResponse(res, `Failed to update the todo with id "${req.params.todoId}"`);
    }
  });

  app.listen(config.apiServerPort, () => {
    console.log(`Express web server started, listening on http://localhost:${config.apiServerPort}`);
  });
}

function createOkResponse<Body>(res: ApiResponse<Body>, body: Body): ApiResponse<Body> {
  return res.status(200).json({
    data: body,
  })
}

function createErrorResponse(res: ApiResponse<unknown>, code: string): ApiResponse<unknown> {
  return res.status(422).json({
    errors: [{code}],
  });
}
