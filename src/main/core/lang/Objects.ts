import {isPlainObject} from 'lodash';
import {DeepReadonly} from 'utility-types';

export function freezeDeep<T>(value: T): DeepReadonly<T> {
  if (Object.isFrozen(value)) {
    return value as DeepReadonly<T>;
  }
  if (Array.isArray(value)) {
    Object.freeze(value);
    for (const item of value) {
      freezeDeep(item);
    }
  } else if (isPlainObject(value)) {
    Object.freeze(value);
    for (const key in value) {
      if ((value as any).hasOwnProperty(key)) {
        freezeDeep(value[key]);
      }
    }
  }
  return value as DeepReadonly<T>;
}
