import {HttpMethod} from '../HttpMethod';
import express from 'express';

export interface IFetchApiRequestOptions<Body extends object> extends Omit<RequestInit, 'body'> {
  method: HttpMethod;
  body?: Body;
}

export interface IApiResponseBody<Data> {
  data?: Data;
  errors?: Array<IApiError>;
}

export type ApiResponse<Data> = express.Response<IApiResponseBody<Data>>;

export interface IApiError {
  code: string;
  message?: string;
}
