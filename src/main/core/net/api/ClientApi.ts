import {HttpMethod, makeApiRequest} from '../../index';
import {ITodo} from '../../../domain';

export const readTodos = () => {
  return makeApiRequest<Array<ITodo>>('/todos');
};

export const createTodo = (description: string) => {
  return makeApiRequest<ITodo, {description: string}>('/todos', {
    method: HttpMethod.POST,
    body: {description},
  })
};

export const toggleTodoCompleted = (id: number) => {
  return makeApiRequest<ITodo>(`/todos/${id}/toggle-completed`, {
    method: HttpMethod.POST,
  });
};
