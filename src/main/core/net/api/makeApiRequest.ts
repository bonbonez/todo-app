import config from '../../../../../config.json';
import {HttpHeader} from '../HttpHeader';
import {HttpContentType} from '../HttpContentType';
import {IApiResponseBody, IFetchApiRequestOptions} from './api-types';

export async function makeApiRequest<ResponseData, RequestBody extends object = never>(path: string, options?: IFetchApiRequestOptions<RequestBody>): Promise<IApiResponseBody<ResponseData>> {
  try {
    const res = await fetch(`http://localhost:${config.apiServerPort}${path}`, {
      ...options,
      body: options?.body ? JSON.stringify(options.body) : undefined,
      headers: {
        [HttpHeader.CONTENT_TYPE]: HttpContentType.JSON,
      }
    });
    return await res.json() as IApiResponseBody<ResponseData>;
  } catch (error) {
    return {errors: [{
      code: 'failed_to_fetch',
      message: error.message,
    }]};
  }
}
