import React from 'react';
import {IStore, Reducer} from './createStore';
import {IAppState} from '../../client/AppModel';

export const StoreContext = React.createContext<IStore<IAppState>>(null as any);

export interface IStoreProviderProps {
  store: IStore<IAppState>;
  children: React.ReactNode;
}

export const StoreProvider: React.FC<IStoreProviderProps> = ({store, children}) => (
    <StoreContext.Provider value={store}>
      {children}
    </StoreContext.Provider>
);

export function useStore<State>(reducer?: Reducer<State>): IStore<IAppState> {
  const store = React.useContext(StoreContext);
  if (reducer) {
    store.addReducer(reducer);
  }
  React.useEffect(() => () => {
    store.removeReducer(reducer);
  }, []);
  return store;
}
