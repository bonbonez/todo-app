import {isEqual} from 'lodash';
import {Objects} from '../index';

export interface IPlainAction<Type = any, Payload = any> {
  type: Type;
  payload?: Payload;
}

export type FunctionAction<GlobalState, Result> = (dispatch: (action: IPlainAction | FunctionAction<GlobalState, Result>) => Result | undefined, getState: IGetState<GlobalState>) => Result;

export type Reducer<State, ActionType extends string = any> = (state: State, action: IPlainAction<ActionType>) => State;

export type StoreListener = () => void;

interface IGetState<GlobalState> {
  (): GlobalState;
  <LocalState>(reducer: Reducer<LocalState>): LocalState;
}

export interface IStore<GlobalState> {
  dispatch: <Result>(action: IPlainAction | FunctionAction<GlobalState, Result>) => Result | undefined;
  getState: IGetState<GlobalState>;
  addReducer: <ReducerState>(reducer: Reducer<ReducerState>) => void;
  removeReducer: <ReducerState>(reducer: Reducer<ReducerState>) => void;
  subscribe: (listener: StoreListener) => void;
  unsubscribe: (listener: StoreListener) => void;
}

export function createStore<GlobalState>(initialState: GlobalState, globalReducer: Reducer<GlobalState> = (state) => state): IStore<GlobalState> {

  const reducers: Map<Reducer<any>, unknown> = new Map();
  const listeners: Set<() => void> = new Set();

  let state: GlobalState = Objects.freezeDeep({...initialState}) as GlobalState;

  const addReducer = (reducer: Reducer<any>) => {
    if (!reducers.has(reducer)) {
      reducers.set(reducer, reducer({}, {type: '@@init'}));
    }
  };

  const removeReducer = (reducer: Reducer<any>) => {
    if (reducers.has(reducer)) {
      reducers.delete(reducer);
    }
  };

  const subscribe = (listener: () => void) => {
    if (!listeners.has(listener)) {
      listeners.add(listener);
    }
  };

  const unsubscribe = (listener: () => void) => {
    if (listeners.has(listener)) {
      listeners.delete(listener);
    }
  };

  const getState = <LocalState>(reducer?: Reducer<LocalState>): GlobalState | LocalState => {
    if (reducer) {
      if (!reducers.has(reducer)) {
        throw new Error(`Reducer ${reducer.name} was not attached to the store`);
      }
      return reducers.get(reducer) as LocalState;
    } else {
      return state as GlobalState;
    }
  }

  const dispatch = <Result>(action: IPlainAction | FunctionAction<GlobalState, Result>): Result | undefined => {

    if (typeof action === 'function') {
      return action(dispatch, getState) as Result;
    }

    let shouldInvokeListeners = false;

    for (const [reducer, localState] of reducers) {
      let newLocalState = reducer(localState, action);
      if (!isEqual(newLocalState, localState)) {
        shouldInvokeListeners = true;
        reducers.set(reducer, Objects.freezeDeep(newLocalState));
      }
    }

    const newState = globalReducer(getState(), action);
    if (!isEqual(newState, getState())) {
      shouldInvokeListeners = true;
      state = Objects.freezeDeep(newState) as GlobalState;
    }

    if (shouldInvokeListeners) {
      for (const listener of listeners) {
        listener();
      }
    }
  };

  dispatch({type: '@@init'});

  return {
    dispatch,
    addReducer,
    removeReducer,
    subscribe,
    unsubscribe,
    getState,
  };
}

export function createAction<Type, Payload>(type: Type, payload: Payload): IPlainAction<Type, Payload> {
  return {type, payload};
}
