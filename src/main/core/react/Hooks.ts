import React from 'react';
import {isEqual} from 'lodash';

export function useRerender(): () => void {
  const [, trigger] = React.useState();
  const fnRef = React.useRef<any>();
  return fnRef.current = fnRef.current || (() => trigger({}));
}

export function useToggle(initialValue = false): [boolean, () => void, () => void, () => void] {
  const [value, setValue] = React.useState(initialValue);
  return [
    value,
    React.useRef(() => setValue(true)).current,
    React.useRef(() => setValue(false)).current,
    React.useRef(() => setValue((value) => !value)).current,
  ];
}

export function useSyncEffect(effect: React.EffectCallback, deps?: React.DependencyList): void {
  const disposeRef = React.useRef<() => void | undefined>();
  const depsRef = React.useRef<React.DependencyList>();

  // TODO: perf bottleneck - change isEqual to isShallowEqual
  if (!deps || !isEqual(depsRef.current, deps)) {
    depsRef.current = deps;

    disposeRef.current?.();
    disposeRef.current = undefined;

    disposeRef.current = effect() || undefined;
  }

  React.useEffect(() => () => disposeRef.current?.(), []);
}
