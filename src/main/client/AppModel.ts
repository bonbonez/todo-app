import {createAction, Reducer} from '../core';

export enum AppStateActionType {
  SET_PENDING = 'AppStateAction.setPending',
}

export interface IAppState {
  pending?: boolean;
}

export const reduceApp: Reducer<IAppState, AppStateActionType> = (state, action) => {
  state = {...state};

  switch (action.type) {

    case AppStateActionType.SET_PENDING:
      state.pending = action.payload.pending;
      break;
  }

  return state;
};

export function setAppPending(pending: boolean) {
  return createAction(AppStateActionType.SET_PENDING, {pending});
}
