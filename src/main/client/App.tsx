import React from 'react';
import {Backdrop, CircularProgress, Container, Grid, makeStyles, Typography} from '@material-ui/core';
import {HighFive, TodoList} from './components';
import {motion} from 'framer-motion';
import {Hooks, useStore} from '../core';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    minWidth: 320,
    minHeight: '100vh',
  },
  app: {
    backgroundColor: theme.palette.primary.main,
    boxSizing: 'border-box',
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'column',
    overflow: 'hidden',
    width: '100%',
  },
  heading: {
    paddingTop: theme.spacing(10),
    paddingBottom: theme.spacing(10),
    textAlign: 'center',
  },
  backdrop: {
    zIndex: 1e9,
  },
}));

export enum AppStep {
  INTRO = 'intro',
  TODO = 'dashboard',
}

export const App: React.FC = () => {

  const classes = useStyles();
  const store = useStore();
  const rerender = Hooks.useRerender();
  const [step, setStep] = React.useState(AppStep.INTRO);
  const appState = store.getState();

  Hooks.useSyncEffect(() => {
    store.subscribe(rerender);
  }, []);

  return (
      <div className={classes.root}>
        <Backdrop
            className={classes.backdrop}
            open={appState.pending || false}
        >
          <CircularProgress/>
        </Backdrop>
        {step === AppStep.INTRO && (
            <HighFive onIntroComplete={() => setStep(AppStep.TODO)}/>
        )}
        {step === AppStep.TODO && (
            <motion.div
                className={classes.app}
                style={{opacity: 0}}
                animate={{opacity: 1}}
            >
              <Container fixed={true}>
                <Grid
                    container={true}
                    wrap="nowrap"
                    justify="center"
                >
                  <Grid
                      item={true}
                      xs={12}
                      sm={9}
                      md={8}
                      lg={6}
                  >
                    <Typography
                        className={classes.heading}
                        variant="h5"
                        color="textSecondary"
                    >
                      {'The TODO App'}
                    </Typography>

                    <TodoList/>

                  </Grid>
                </Grid>
              </Container>
            </motion.div>
        )}
      </div>
  );
};
