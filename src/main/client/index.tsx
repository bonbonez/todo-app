import React from 'react';
import ReactDOM from 'react-dom';
import {ThemeProvider} from '@material-ui/core';
import {SnackbarProvider} from 'notistack'
import {createStore, StoreProvider} from '../core';
import {App} from './App';
import {IAppState, reduceApp} from './AppModel';
import 'reset-css';
import {createTheme} from '../domain';

const theme = createTheme();
const store = createStore<IAppState>({}, reduceApp);

ReactDOM.render(
    <StoreProvider store={store}>
      <ThemeProvider theme={theme}>
        <SnackbarProvider maxSnack={3}>
          <App/>
        </SnackbarProvider>
      </ThemeProvider>
    </StoreProvider>,
    document.getElementById('root')
);

module.hot?.accept('*', window.location.reload);
