import React from 'react';
import {motion} from 'framer-motion';
import {makeStyles, Typography, useTheme} from '@material-ui/core';
import {Hooks} from '../../../core';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flex: '1 1 0',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100vh',
    overflow: 'hidden',
    width: '100vw',
  },
  animation: {
    cursor: 'pointer',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
    textAlign: 'center',
  },
  icon: {
    paddingTop: theme.spacing(1),
  }
}));

export interface IHighFiveProps {
  onIntroComplete: () => void;
}

export const HighFive: React.FC<IHighFiveProps> = ({onIntroComplete}) => {

  const classes = useStyles();
  const theme = useTheme();
  const [hovered, setHovered, setNotHovered] = Hooks.useToggle(false);
  const [active, setActive] = Hooks.useToggle(false);

  const handleClick = () => {
    if (hovered) {
      setActive();
    } else {
      setHovered();
    }
  };

  const handleAnimationComplete = () => {
    if (active) {
      onIntroComplete();
    }
  };

  return (
      <div className={classes.root}>
        <motion.div
            className={classes.animation}
            onHoverStart={setHovered}
            onHoverEnd={setNotHovered}
            onClick={handleClick}
            initial={false}
            animate={active ? 'fullscreen' : hovered ? 'hovered' : 'intact'}
            onAnimationComplete={handleAnimationComplete}
            variants={{
              intact: {
                borderRadius: 50,
                height: theme.spacing(15),
                scale: 1,
                width: theme.spacing(15),
              },
              hovered: {
                backgroundColor: theme.palette.primary.main,
                scale: 4,
                rotate: 360,
              },
              fullscreen: {
                borderRadius: 0,
                backgroundColor: theme.palette.background.default,
                scale: 3,
                height: '100%',
                width: '100%',
              }
            }}
        >
          <motion.span
              className={classes.icon}
              initial={false}
              animate={active ? 'hidden' : 'visible'}
              variants={{
                visible: {opacity: 1},
                hidden: {opacity: 0, scale: 0},
              }}
          >
            <Typography variant="h4">
              {'👋'}
            </Typography>
          </motion.span>
        </motion.div>
      </div>
  );
};

HighFive.displayName = 'HighFive';
