import React from 'react';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import {Hooks} from '../../../core';

dayjs.extend(relativeTime)

export interface IRelativeTimeProps {
  date: string | Date | number;
  className?: string;
  tagName?: string;
}

export const RelativeTime: React.FC<IRelativeTimeProps> = (props) => {
  const {
    date,
    className,
    tagName = 'span'
  } = props;

  const rerender = Hooks.useRerender();

  React.useEffect(() => {
    const intervalId = setInterval(rerender, 1000);
    return () => clearInterval(intervalId);
  });

  return React.createElement(
      tagName,
      {className},
      dayjs().to(React.useMemo(() => dayjs(date), [date])),
  );
};
