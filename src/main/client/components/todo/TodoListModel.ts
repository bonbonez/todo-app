import {ClientApi, createAction, FunctionAction, Reducer} from '../../../core';
import {ITodo} from '../../../domain';
import {IAppState, setAppPending} from '../../AppModel';

export enum TodoListActionType {
  ADD_TODO = 'TodoListAction.addTodo',
  POPULATE_TODOS = 'TodoListAction.populateTodos',
}

export interface ITodoListState {
  todos: Array<ITodo>
}

export const reduceTodoList: Reducer<ITodoListState, TodoListActionType> = (state, action) => {
  state = {...state};

  if (!state.todos) {
    state.todos = [];
  }

  switch (action.type) {

    case TodoListActionType.ADD_TODO:
      state.todos = [...state.todos, action.payload];
      break;

    case TodoListActionType.POPULATE_TODOS:
      state.todos = [...action.payload];
      break;
  }
  return state;
};

export const addTodo = (todo: ITodo) => {
  return createAction(TodoListActionType.ADD_TODO, todo);
}

export const populateTodos = (todos: Array<ITodo>) => {
  return createAction(TodoListActionType.POPULATE_TODOS, todos);
};

export function readTodos(): FunctionAction<IAppState, Promise<boolean>> {
  return async (dispatch, getState) => {
    const response = await ClientApi.readTodos();
    if (response.data) {
      dispatch(populateTodos(response.data));
    }
    return !!response.data;
  };
}

export function createTodo(description: string): FunctionAction<IAppState, Promise<boolean>> {
  return async (dispatch, getState) => {

    dispatch(setAppPending(true));

    // Simulate long-going API request
    await timeoutPromise(500);
    const response = await ClientApi.createTodo(description);
    if (response.data) {
      dispatch(addTodo(response.data));
      await dispatch(readTodos())
    }

    dispatch(setAppPending(false));
    return !!response.data;
  };
}

export function toggleTodoCompleted(todoId: number): FunctionAction<IAppState, Promise<boolean>> {
  return async (dispatch, getState) => {
    dispatch(setAppPending(true));

    // Simulate long-going API request
    await timeoutPromise(500);

    const response = await ClientApi.toggleTodoCompleted(todoId);
    if (response.data) {
      await dispatch(readTodos());
    }
    dispatch(setAppPending(false));
    return !!response.data;
  };
}

function timeoutPromise(timeout: number): Promise<number> {
  return new Promise((resolve) => setTimeout(resolve, timeout));
}
