import React, {KeyboardEventHandler} from 'react';
import {Hooks, useStore} from '../../../core';
import {Button, Grid, makeStyles, TextField} from '@material-ui/core';
import {createTodo, readTodos, reduceTodoList, toggleTodoCompleted} from './TodoListModel';
import {motion} from 'framer-motion';
import {TodoListItem} from './TodoListItem';
import {useSnackbar} from 'notistack';
import {ITodo} from '../../../domain';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
  },
  submit: {
    marginTop: theme.spacing(4),
  },
  list: {
    paddingTop: theme.spacing(10),
  },
}));

export const TodoList: React.FC = () => {

  const classes = useStyles();
  const store = useStore(reduceTodoList);
  const snackbar = useSnackbar();

  const {todos} = store.getState(reduceTodoList);
  const [description, setDescription] = React.useState('');
  const {pending} = store.getState();

  const showError = (message: string) => {
    const key = snackbar.enqueueSnackbar(message, {
      variant: 'error',
      autoHideDuration: 3000,
      onClick: () => {
        snackbar.closeSnackbar(key);
      }
    })
  };

  Hooks.useSyncEffect(() => {
    const init = async () => {
      if (!await store.dispatch(readTodos())) {
        showError('Failed to fetch the list of todos. Is server running?');
      }
    };
    init();
  }, []);

  const handleKeyDown: KeyboardEventHandler<HTMLInputElement> = (event) => {
    if (description && event.key === 'Enter') {
      handleSubmit();
    }
  };

  const handleSubmit = async () => {
    if (!await store.dispatch(createTodo(description))) {
      showError('Failed to create todo. Is server running?');
    }
  };

  const handleTodoListItemClick = async (todo: ITodo) => {
    if (!await store.dispatch(toggleTodoCompleted(todo.id))) {
      showError('Failed to toggle completed state. Is server running?');
    }
  };

  return (
      <div className={classes.root}>

        <TextField
            value={description}
            onChange={(event) => setDescription(event.target.value)}
            label="What's needed to be done?"
            onKeyDown={handleKeyDown}
            disabled={pending}
            variant="outlined"
            color="secondary"
        />

        <Button
            className={classes.submit}
            variant="contained"
            color="secondary"
            disabled={!description || pending}
            onClick={handleSubmit}
        >
          {'Add to the list'}
        </Button>

        {todos.length > 0 && (
            <Grid
                className={classes.list}
                container={true}
                spacing={5}
            >
              {todos.map((todo, index) => (
                  <Grid
                      item={true}
                      xs={12}
                      sm={6}
                      md={4}
                      key={index}
                  >
                    <motion.div
                        animate={todo.completed ? 'completed' : 'active'}
                        variants={{
                          completed: {opacity: 0.7},
                          active: {opacity: 1},
                        }}
                    >
                      <TodoListItem
                          todo={todo}
                          onClick={() => handleTodoListItemClick(todo)}
                      />
                    </motion.div>
                  </Grid>
              ))}
            </Grid>
        )}
      </div>
  );
};

TodoList.displayName = 'TodoList';
