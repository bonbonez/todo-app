import React from 'react';
import {makeStyles, Paper, Typography} from '@material-ui/core';
import {RelativeTime} from '../relative-time';
import classNames from 'classnames';
import {ITodo} from '../../../domain';

const useStyles = makeStyles((theme) => ({
  root: {
    cursor: 'pointer',
    textAlign: 'left',
    padding: theme.spacing(4),
    opacity: 0.9,
    transitionProperty: 'opacity',
    transitionDuration: '0.3s',
    transitionTimingFunction: 'ease',

    '&:hover': {
      '&:not($completed)': {
        opacity: 1,
      },
      '&$completed': {
        opacity: 0.6,
      }
    },
  },
  completed: {
    opacity: 0.5,
  },
  content: {
    position: 'relative',
  },
  strokeOut: {
    textDecoration: 'line-through',
  },
  createdLabel: {
    paddingTop: theme.spacing(3),
    opacity: 0.5,
  },
}));

export interface ITodoListItemProps {
  todo: ITodo;
  onClick?: () => void;
}

export const TodoListItem: React.FC<ITodoListItemProps> = ({todo, onClick}) => {
  const classes = useStyles();
  return (
      <Paper
          className={classNames(classes.root, todo.completed && classes.completed)}
          onClick={onClick}
          elevation={1}
      >
        <Typography
            className={todo.completed ? classes.strokeOut : undefined}
            variant="body2"
        >
          {todo.description}
        </Typography>

        <Typography
            className={classes.createdLabel}
            variant="caption"
        >
          <RelativeTime date={todo.createdAt}/>
        </Typography>
      </Paper>
  );
};

TodoListItem.displayName = 'TodoListItem';
