export interface ITodo {
  id: number;
  description: string;
  createdAt: string;
  updatedAt: string;
  completed: boolean;
}
