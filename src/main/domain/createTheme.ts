import {createMuiTheme, Theme} from '@material-ui/core';

export function createTheme(): Theme {
  return createMuiTheme({
    spacing: 4,
    shape: {
      borderRadius: 5,
    },
    palette: {
      primary: {
        main: '#29b6f6',
        light: '#73e8ff',
        dark: '#0086c3',
      },
      secondary: {
        main: '#673ab7',
        light: '#9a67ea',
        dark: '#320b86',
      },
      background: {
        default: '#fcfcfc',
      },
      text: {
        primary: '#000000',
        secondary: '#ffffff',
      },
      warning: {
        main: '#ffe66d',
      }
    },
    typography: {
      fontFamily: 'Open Sans',
      fontSize: 16,
      fontWeightLight: 300,
      fontWeightRegular: 400,
      fontWeightMedium: 600,
      fontWeightBold: 700
    }
  });
}
