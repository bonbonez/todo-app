import {createStore, IPlainAction, Reducer} from '../../../main/core';

interface ITestState {
  foo?: number;
}

// TODO: add 100500 more tests
describe('createStore', () => {

  it('creates store and provides a way to get a state', () => {
    const s = createStore({foo: 123});
    expect(s.getState()).toEqual({foo: 123});
  });

  it('invokes an initial reducer right away', () => {
    const s = createStore<ITestState>({}, () => ({foo: 123}));
    expect(s.getState()).toEqual({foo: 123});
  });

  it('does not call a listener if the state remains intact after dispatch', () => {
    const s = createStore<ITestState>({});
    const listener = jest.fn();
    s.subscribe(listener);
    s.dispatch({type: 'foo'});
    expect(listener).not.toHaveBeenCalled();
  });

  it('calls a listener if the state changes when an action is dispatch', () => {
    const reducer = (state: ITestState, action: IPlainAction<any, {foo: number}>) => {
      return {foo: action.type === 'foo' ? action.payload.foo : 123};
    };
    const s = createStore<ITestState>({}, reducer);
    const listener = jest.fn();
    s.subscribe(listener);
    s.dispatch({type: 'foo', payload: {foo: 777}});

    expect(s.getState()).toEqual({foo: 777});
    expect(listener).toHaveBeenCalledTimes(1);
  });

  it('returns a deep frozen state', () => {
    const s = createStore({foo: [{bar: {baz: 123}}]});
    expect(Object.isFrozen(s.getState().foo[0].bar)).toBe(true);
  });

  it('may attach reducers on the go', () => {
    const s = createStore({foo: 123});
    const reducer: Reducer<{bar: number}> = () => ({bar: 123});
    s.addReducer(reducer);

    expect(s.getState(reducer)).toEqual({bar: 123});
  });

  it('executes actions synchronously inside function actions', () => {
    const reducer: Reducer<{foo?: number}> = () => ({foo: 123});
    const s = createStore({}, reducer);

    s.dispatch((dispatch, getState) => {
      dispatch({type: ''});
      expect(getState()).toEqual({foo: 123});
    });
  });
});
