import {Objects} from '../../../main/core';

describe('freezeDeep', () => {
  it('freezes object', () => {
    expect(Object.isFrozen(Objects.freezeDeep({}))).toBe(true);
  });

  it('returns original object', () => {
    const a = {};
    expect(Objects.freezeDeep(a)).toBe(a);
  });

  it('returns original array', () => {
    const a: any = [];
    expect(Objects.freezeDeep(a)).toBe(a);
  });

  it('freezes nested objects', () => {
    const a = {foo: {bar: {}}};
    Objects.freezeDeep(a);

    expect(Object.isFrozen(a)).toBe(true);
    expect(Object.isFrozen(a.foo)).toBe(true);
    expect(Object.isFrozen(a.foo.bar)).toBe(true);
  });

  it(`doesn't freeze class instances`, () => {
    class Foo {}

    const foo = Objects.freezeDeep(new Foo());
    expect(Object.isFrozen(foo)).toBe(false);
  });

  it(`doesn't freeze nested class instances`, () => {
    class Foo {}

    const a = {foo: new Foo()};
    Objects.freezeDeep(a);
    expect(Object.isFrozen(a)).toBe(true);
    expect(Object.isFrozen(a.foo)).toBe(false);
  });

  it(`doesn't freeze class instances in array`, () => {
    class Foo {}

    const a = [new Foo()];
    Objects.freezeDeep(a);
    expect(Object.isFrozen(a)).toBe(true);
    expect(Object.isFrozen(a[0])).toBe(false);
  });

  it(`freezes objects with cyclic dependencies`, () => {
    const a = {b: {c: null as any, d: {foo: 123, bar: 234}}};
    a.b.c = a as any;
    Objects.freezeDeep(a);
    expect(Object.isFrozen((a.b.c as any).a)).toBe(true);
    expect(Object.isFrozen(a.b.d)).toBe(true);
  });
});
