#### Overview

This is the demo of TODO app! 😆 Yet another TODO app. The app:
- Provides a way to add TODOs.
- Stores all added TODOs in a persistent database.

The demo comprises two major parts - the *server app* and the *client app*.

The client app is built using `webpack`, `React` and `TypeScript`. Everything is written from scratch. The task was a perfect opportunity to try my skills and write a redux-like store ([see here](src/main/core/store/createStore.ts)) which is used in the app.

The server app uses express to run the API server. The app uses `sqlite` datebase so no pre-configuration is needed to set up the project, as the db is created upon the initial run. `sequelize` ORM is used to work with the database.

The test task was written in about 25 hours (like in a hackathon manner :). The project structure leaves a lot to be desired. If I had more time, I would have set up `lerna` and added way more tests.

#### Installation

```shell script
# install dependencies
npm install

# run the client 
npm run client

# open a separate tab and run the server
npm run server
# This command will:
# - create the database if it does not exist
# - start express API server
```

Open http://localhost:8080
