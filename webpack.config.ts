import {Configuration} from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import {CleanWebpackPlugin} from 'clean-webpack-plugin';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import {compact} from 'lodash';
import path from 'path';

export default (env: any, argv: Pick<Configuration, 'mode'> = {}) => {

  const prod = argv.mode === 'production';
  const ifDev = <T>(value: T): T | undefined => prod ? undefined : value;

  const targetDir = path.resolve(__dirname, 'target/out');
  const srcDir = path.resolve(__dirname, 'src/main/client');

  return {
    mode: prod ? 'production' : 'development',
    devtool: 'source-map',
    context: srcDir,
    resolve: {extensions: ['.ts', '.tsx', '.js', '.jsx']},

    devServer: ifDev({
      contentBase: targetDir,
      hot: true,
    }),

    entry: path.resolve(srcDir, 'index.tsx'),

    output: {
      filename: 'js/bundle.[hash].min.js',
      path: targetDir,
      publicPath: '/',
    },

    module: {
      rules: [
        {
          test: /\.tsx?$/,
          exclude: /[\\/]node_modules[\\/]/,
          use: [
            {
              loader: 'ts-loader',
              options: {
                configFile: path.join(__dirname, 'tsconfig.json'),
                transpileOnly: true,
                silent: true,
              },
            },
          ],
        },
        {
          test: /\.css$/,
          use: ['style-loader', {loader: 'css-loader', options: {importLoaders: 1}}],
        },
        {
          test: /\.(jpe?g|png|gif|woff|woff2|eot|ttf|svg)$/i,
          loader: 'file-loader',
        }
      ],
    },

    plugins: compact([
      ifDev(new CleanWebpackPlugin()),

      new HtmlWebpackPlugin({
        template: 'index.html',
      }),

      new CopyWebpackPlugin({
        patterns: [{
          from: path.resolve(srcDir, 'static'),
        }],
      }),
    ])
  };
};
